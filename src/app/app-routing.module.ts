import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { HomeComponent } from './home/home.component'
import { DemoLandingComponent } from './_demo-core/demo-landing/demo-landing.component'
import { ConfigGuard } from './_demo-core/config.guard'
import { GuardedRouteComponent } from 'src/app/guarded-route/guarded-route.component'
import { AuthGuard } from 'src/app/auth.guard'
import { PasswordResetComponent } from 'src/app/password-reset/password-reset.component'

const routes: Routes = [
	{ path: '', component: DemoLandingComponent },
	{ path: 'home', component: HomeComponent,  canActivate: [ConfigGuard]},
	{ path: 'reset-password', component: PasswordResetComponent, canActivate: [ConfigGuard]},
	{ path: 'guarded-route', component: GuardedRouteComponent, canActivate: [ConfigGuard, AuthGuard] },
]

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {
}
