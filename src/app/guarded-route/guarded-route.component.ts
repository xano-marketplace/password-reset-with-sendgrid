import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/_demo-core/config.service'

@Component({
  selector: 'app-guarded-route',
  templateUrl: './guarded-route.component.html',
  styleUrls: ['./guarded-route.component.scss']
})
export class GuardedRouteComponent implements OnInit {

  constructor(
    private configService: ConfigService
  ) { }

  public user;

  ngOnInit(): void {
    this.configService.xanoAPI(
      '/auth/me',
      'get',
      null,
      {},
      true
    ).subscribe(res => this.user = res)
  }


}
