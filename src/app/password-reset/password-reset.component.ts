import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ConfigService } from 'src/app/_demo-core/config.service'
import { ControlSchema } from 'src/app/_demo-core/form-generator/form-generator.component'

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html'
})
export class PasswordResetComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private configService: ConfigService
  ) {
  }

  public token;
  public form;
  public resetFormSchema: ControlSchema[] = [
    {name: 'password', type: 'password', label: 'Password', icon: 'lock', validators: ['required']},
    {name: 'passwordConfirm', type: 'password', label: 'Confirm Password', icon: 'lock',
      validators: ['required', 'matches|password']},
  ];

  ngOnInit(): void {
    this.route.queryParams.subscribe(res => {
      if (!res?.token) {
        this.router.navigate(['home']);
      } else {
        this.token = res.token;
      }
    });
  }

  public resetPassword() {
    this.form.markAllAsTouched()
    if (this.form.valid && this.token) {
      this.configService.xanoAPI('/auth/password-reset', 'post', {
        reset_token: this.token,
        new_password: this.form.controls.password.value,
      }).subscribe(token => {
          this.configService.authToken.next(token.authToken);
          this.router.navigate(['guarded-route'])
        },
        error => this.configService.showErrorSnack(error))
    } else if (!this.token) {
      this.configService.showErrorSnack({ error: { message: 'Token missing from params' } })
    }
  }
}
