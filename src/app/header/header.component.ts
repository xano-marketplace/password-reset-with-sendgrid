import {Component, OnInit} from '@angular/core';
import {ConfigService} from '../_demo-core/config.service';
import {FormControl} from '@angular/forms';
import {DemoConfig} from '../_demo-core/_demo-core';
import { Router } from '@angular/router'

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
	constructor(
		private configService: ConfigService,
		private router: Router
	) {
	}

	public apiConfigured: boolean;
	public loggedIn: boolean;
	public config: DemoConfig;

	ngOnInit(): void {
		this.config = this.configService.config;
		this.configService.isConfigured().subscribe(apiUrl => this.apiConfigured = !!apiUrl);
		this.configService.isLoggedIn().subscribe(res => this.loggedIn = res);
	}

	public logout(){
		this.configService.authToken.next(null);
		this.router.navigate(['home'])
	}
}
