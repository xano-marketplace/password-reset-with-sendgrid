import {Injectable} from '@angular/core';
import {BehaviorSubject, EMPTY, Observable} from 'rxjs';
import {XanoService} from './xano.service';
import {config} from './config';
import {get} from 'lodash-es';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpClient} from '@angular/common/http';
import {ColumnSearchExpression, DemoConfig, Expression, GeoSearchExpression} from './_demo-core';

@Injectable({
	providedIn: 'root'
})

export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public authToken: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public config: DemoConfig = config;

	constructor(
		private http: HttpClient,
		private xanoService: XanoService,
		private snackBar: MatSnackBar) {
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public isLoggedIn(): Observable<any> {
		return this.authToken.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.http.get(this.xanoService.getApiSpecUrl(apiUrl), {
			headers: {Accept: 'text/yaml'},
			responseType: 'text'
		});
	}

	public showErrorSnack(error): void {
		this.snackBar.open(get(error, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
	}

	public xanoAPI(endpoint: any, method: string, body: any = null, options: any = {}, authenticate: boolean = false): Observable<any> {
		if (authenticate) {
			if (options?.headers) {
				options.headers.Authorization = 'Bearer' + this.authToken.value;
			} else {
				options.headers = {Authorization: `Bearer ${this.authToken.value}`};
			}
		}

		switch (method.toUpperCase()) {
			case 'GET':
				return this.http.get(this.xanoApiUrl.value + endpoint, options);
			case 'POST':
				return this.http.post(this.xanoApiUrl.value + endpoint, body, options);
			case 'PUT':
				return this.http.put(this.xanoApiUrl.value + endpoint, body, options);
			case 'DELETE':
				return this.http.delete(this.xanoApiUrl.value + endpoint, options);
			default:
				return EMPTY;
		}
	}


	public searchExpressionBuilder(expressions: Expression[], asGroup: boolean, orGroup: boolean): any {
		const statements = [];

		for (const expression of expressions) {
			const statement = {
				statement: {
					left: {
						tag: expression.left.tag,
						operand: expression.left.operand
					},
					right: {
						operand: expression.right.operand
					}
				}
			};

			if (expression.left.filters?.length) {
				Object.assign(statement.statement.left, {filters: expression.left.filters});
			}

			if (expression.operator) {
				Object.assign(statement.statement, {op: expression.operator});
			}

			if (statements.length && expression.or) {
				Object.assign(statement, {or: true});
			}

			if (expression.right.tag) {
				Object.assign(statement.statement.right, {tag: expression.right.tag});
			}

			if (expression.right.filters?.length) {
				Object.assign(statement.statement.right, {filters: expression.right.filters});
			}

			if (expression.right.ignore_empty) {
				Object.assign(statement.statement.right, {ignore_empty: expression.right.ignore_empty});
			}

			statements.push(statement);
		}

		if (asGroup) {
			return {
				type: 'group',
				or: orGroup,
				group: {
					expression: statements
				}
			};
		} else {
			return statements;
		}
	}


	public searchColumn(columnSearchExpressions: ColumnSearchExpression[], asGroup = false, orGroup = false): any {
		const expressions = [];

		for (const expression of columnSearchExpressions) {
			expressions.push({
				left: {
					tag: 'col',
					operand: expression.column
				},
				operator: expression.operator,
				right: {
					operand: expression.query
				},
				or: expression.or
			});
		}
		return this.searchExpressionBuilder(expressions, asGroup, orGroup);
	}

	public geoLocationSearch(geoSearchExpressions: GeoSearchExpression[], asGroup = false, orGroup = false): any {
		const expressions = [];

		for (const expression of geoSearchExpressions) {
			expressions.push({
				or: expression.or,
				left: {
					tag: 'col',
					operand: expression.column,
					filters: [{
						name: 'within',
						arg: [
							{
								tag: 'const',
								value: {
									type: 'point',
									data: {
										lat: expression.lat,
										lng: expression.lng
									}
								}
							}, {
								tag: 'const',
								value: expression.radius
							}
						]
					},
					]
				},
				right: {
					operand: true
				}
			});
		}
		return this.searchExpressionBuilder(expressions, asGroup, orGroup);
	}

}
