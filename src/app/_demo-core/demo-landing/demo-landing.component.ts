import {Component, OnInit, ViewChild} from '@angular/core';
import {ConfigService} from '../config.service';
import {MatAccordion} from '@angular/material/expansion';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {parse as yaml2JSON} from 'yamljs';
import {DemoConfig} from '../_demo-core';
import {config} from '../config';

@Component({
	selector: 'app-demo-landing',
	templateUrl: './demo-landing.component.html',
	styleUrls: ['./demo-landing.component.scss']
})

export class DemoLandingComponent implements OnInit {
	public config: DemoConfig = config;
	@ViewChild(MatAccordion) accordion: MatAccordion;
	public configForm: FormGroup = new FormGroup({
		xanoUrl: new FormControl(this.configService.xanoApiUrl.value,
			[Validators.required, Validators.pattern('^https?://.+')]
		)
	});

	constructor(
		private configService: ConfigService,
		private router: Router
	) {
		this.config = configService.config;
	}

	ngOnInit(): void {
	}


	public submit(): void {
		this.configForm.markAllAsTouched();
		if (this.configForm.valid) {
			this.configService.configGet(this.configForm.controls.xanoUrl.value)
				.subscribe(res => {
						if (!this.configService.config.requiredApiPaths.every(path => res.includes(path))) {
							const message = 'This Xano Base URL is missing the required endpoints. Have you installed this marketplace extension?';
							this.configService.showErrorSnack({error: {message}});
						} else {
							this.configService.xanoApiUrl.next(this.configForm.controls.xanoUrl.value);
							localStorage.setItem('xanoUrl', this.configForm.controls.xanoUrl.value)
							this.router.navigate(['home']);
						}
					}, error => this.configService.showErrorSnack(error)
				);
		}
	}

	public pasteSubmit(): void {
		setTimeout(() => {
			this.submit();
		}, 100);
	}
}
