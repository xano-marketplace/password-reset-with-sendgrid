import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core'
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms'

// types = ['text', 'password', 'email', 'number',
// hidden, checkbox, radio, date, time, datetime, range, telephone, money, color, url, upload];
// views = [input, radio, checkbox, textarea, tagInput, toggleButton, select, slider, toggleSwitch, autocomplete]

export interface ControlSchema {
	name: string;
	type: string;
	state?: any;
	label?: string;
	hint?: string;
	disabled?: boolean;
	order?: number;
	icon?: string;
	icon_placement?: string;
	placeholder?: string;
	view?: string;
	value_options?: any[];
	validators?: any[];
	textarea_rows?: number;
}
@Component({
	selector: 'app-form-generator',
	templateUrl: './form-generator.component.html',
	styleUrls: ['./form-generator.component.scss']
})
export class FormGeneratorComponent implements OnInit, OnChanges {
	constructor() {
	}

	private emailRegex = new RegExp('^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$');
	public uploading: boolean;
	public form: FormGroup = new FormGroup({});

	@Input() flexDirection: string;
	@Input() appearance: any = 'outline';
	@Input() schema: ControlSchema[] = [];
	@Input() formValues: any;
	@Output() formOutput = new EventEmitter<any>();

	ngOnInit(): void {
		// this.generateForm();
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.generateForm();
	}

	private generateForm(): void {
		for (const x of this.schema) {
			this.form.addControl(x.name, new FormControl(x.state));
			if (x.validators) {
				this.form.get(x.name).setValidators(this.generateValidators(x.validators));
			}


			// disable toggle
			x.disabled ? this.form.get(x.name).disable() : this.form.get(x.name).enable();
		}

		if (this.formValues) {
			this.form.patchValue(this.formValues);
		}

		this.formOutput.emit(this.form);

		this.form.valueChanges.subscribe(res => {
			this.formOutput.emit(this.form);
		});
	}

	private generateValidators(validators): ValidatorFn[] {
		const validatorsArray: ValidatorFn[] = [];
		for (const validator of validators) {
			if (typeof validator === 'string') {
				if (validator.startsWith('matches')) {
					validatorsArray.push(this.matches(validator.split('|')[1]));
				} else {
					switch (validator) {
						case 'required':
							validatorsArray.push(Validators.required);
							break;
						case 'email':
							validatorsArray.push(Validators.pattern(this.emailRegex));
							break;
						default:
							break;
					}
				}
			} else if (typeof validator === 'function') {
				validatorsArray.push(validator);
			}
		}
		return validatorsArray;
	}

	public getErrorMessage(controlName): string {
		const control = this.form.get(controlName);

		if (control.hasError('required')) {
			return controlName + ' is required.';
		}

		if (control.hasError('matches')) {
			return controlName + ' and ' +
			this.schema
				.find(x => x.name === controlName)?.validators
				.find(x => typeof x === 'string' && x.includes('matches')).split('|')[1] +
				' do not match!'
		}

		if (control.hasError('pattern')) {
			return controlName.toLowerCase().includes('email') ? 'Not a valid email.' : 'Not a valid pattern.';
		}

		if (control.hasError('minlength')) {
			return controlName + ' has a minimum length of ' + control.errors.minlength.requiredLength;
		}

		if (control.hasError('maxlength')) {
			return controlName + ' has a maximum length of ' + control.errors.maxlength.requiredLength;
		}

		return 'Invalid Input';
	}

	public matches(matchTo: string): (AbstractControl) => ValidationErrors | null {
		return (control: AbstractControl): ValidationErrors | null => {
			return !!control.parent &&
			!!control.parent.value &&
			control.value === control.parent.controls[matchTo].value
				? null : { matches: true }
		}
	}

}
