import { DemoConfig } from './_demo-core'

export const config: DemoConfig = {
	title: 'Password Reset',
	summary: '',
	marketplace_type: 'extension',
	editLink: 'https://gitlab.com/xano-marketplace/password-reset-with-sendgrid/',
	components: [
		{
			name: 'Home',
			description: 'This component contains a card where you can login, request a password reset link, or create a new account.',
		},
		{
			name: 'Password Reset',
			description: 'This component is the redirect URI for the reset link. It contains a form with a new password input and a confirm new password input',
		},
		{
			name: 'Guarded Route',
			description: 'This component is only accessible if authenticated. It will GET /auth/me by passing back the auth token and show the user\'s name to demonstrate that everything worked properly during the authentication process.',
		},

	],
	instructions: [
		'This extension uses <a href="https://sendgrid.com/" target="_blank">Sendgrid</a> so you will need <a href="https://signup.sendgrid.com/" target="_blank">Sendgrid account</a> ',
		'In Sendgrid, if you have not already, set up an outgoing email address. From the Dashboard, select <b>Create a Single Sender</b> or authenticate a domain instead. Make sure to verify the email address',
		`In Sendgrid, on the left side menu, go to <b>Email API –> Dynamic Templates</b> and create a new template, then in the templates list, select the newly created template, 
			and click <b></b>. Next, select Blank Template and click Code Editor to design your template. (Note: The only requirement is that you add an anchor tag 
			with HREF set to reset_link i.e. <code> &#8249;a href="{{{reset_link}}}" target="_blank"> Reset Password</a&#8250;</code>`,
		'You will need a Sendgrid API key, so on the left side menu in Sendgrid, go to <b>Settings –> API Keys</b> and create one, then copy the API key and save it somewhere safe.' +
		'(Note: you must copy the key before leaving the page, otherwise, you will have to create another key). creating the api key, otherwise you will need to create another one)',
		`Next, go to the Password Reset extension page in your Xano workspace and click the Configure button.`,
		'Update environment variable sendgrid_from_email with the outgoing email from step 2, ' +
		'update <b>sendgrid_reset_link_template</b> with the template id created in step 3 and update <b>sendgrid_api_key</b> with the API Key from step 4.',
		'Set your environment variable <b>reset_link_redirect_uri</b> to the front-end route that will handle passing the token back to Xano. (It is auto-populated with the redirect URI for the live demo).',
		'To set up the environment variable <b>password_reset_jwt_secret</b>, navigate to <b>Library –> Functions</b> ' +
		'in the left-hand sidebar in your Xano workspace and select <b>generate_jwt_secret</b>. Next, click the <b>Run & Debug</b> button on the top right and click <b>Run</b> in the panel. Copy the JSON result, and finally, navigate back to your environment variables and paste this value as <b>magic_jwt_secret.</b>',
		'(Optional) if you want a different expiration time for the password reset link, you can set reset_link_expiry_time to another value. (The default is 3600 seconds or one hour).',
		'Go to the newly added Reset Link API Group and copy your API BASE URL',
		'In this demo, paste this API BASE URL into "Your Xano API URL."',
		'Login, or click forgot password, for an existing user by providing your email address or sign up as a new user by selecting Register Now.'
	],
	requiredApiPaths: [
		'/auth/login',
		'/auth/signup',
		'/auth/me',
		'/auth/password-reset',
		'/auth/password-reset-link',
	]
};

