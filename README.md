# Xano Password Reset

This demo illustrates the reset password process utilizing emails. The extension uses sendgrid as the email service
for sending the reset link email.


### Setup & Dependencies

#### List of Dependencies
* material
* bootstrap css
* lodash-es

#### Setup

You can use either npm or yarn to install dependencies.

run `npm install` or `yarn`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
